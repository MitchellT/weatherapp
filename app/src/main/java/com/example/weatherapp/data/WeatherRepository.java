package com.example.weatherapp.data;

import com.example.weatherapp.data.model.WeatherResponse;

import io.reactivex.Observable;

public interface WeatherRepository {

    Observable<WeatherResponse> getWeatherForecast();

}
