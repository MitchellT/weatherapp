package com.example.weatherapp.data.remote;

import com.example.weatherapp.data.model.WeatherResponse;

import java.util.List;


import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface WeatherService {

    @GET("forecast?zip=30320")
    Observable<WeatherResponse> weatherReports(@Query("appid") String api_key);

}
