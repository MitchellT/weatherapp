package com.example.weatherapp.data.remote;

import com.example.weatherapp.data.model.WeatherResponse;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class ApiHelperImpl implements ApiHelper {

    private final String API_KEY = "4562ed248ccd721ff128aa26326b9ca7";

    @Override
    public Observable<WeatherResponse> getWeatherApiObservable() {
        WeatherService service = RetrofitInstance.getRetrofitInstance().create(WeatherService.class);
        return service.weatherReports(API_KEY);
    }


}
