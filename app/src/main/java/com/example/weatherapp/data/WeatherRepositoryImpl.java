package com.example.weatherapp.data;

import com.example.weatherapp.data.model.WeatherResponse;
import com.example.weatherapp.data.remote.ApiHelper;
import com.example.weatherapp.data.remote.ApiHelperImpl;
import com.example.weatherapp.data.remote.RetrofitInstance;
import com.example.weatherapp.data.remote.WeatherService;

import io.reactivex.Observable;
import retrofit2.Call;

public class WeatherRepositoryImpl implements WeatherRepository {

    private ApiHelper apiHelper;

    public WeatherRepositoryImpl() {
        apiHelper = new ApiHelperImpl();
    }

    @Override
    public Observable<WeatherResponse> getWeatherForecast() {
        // Would grab from local storage if present there
        return apiHelper.getWeatherApiObservable();
    }


}
