package com.example.weatherapp.data.remote;

import com.example.weatherapp.data.model.WeatherResponse;

import io.reactivex.Observable;

public interface ApiHelper {

    Observable<WeatherResponse> getWeatherApiObservable();

}
