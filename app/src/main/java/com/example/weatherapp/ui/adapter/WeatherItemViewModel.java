package com.example.weatherapp.ui.adapter;

import com.example.weatherapp.data.model.WeatherResponse;
import com.example.weatherapp.utils.CommonUtils;

import androidx.databinding.ObservableField;

public class WeatherItemViewModel {

    public final ObservableField<String> date;

    public final ObservableField<String> maxTemp;

    public final ObservableField<String> minTemp;

    public final ObservableField<String> forecastDesc;

    private final WeatherResponse.List mWeatherItem;

    public WeatherItemViewModel(WeatherResponse.List item) {
        this.mWeatherItem = item;
        date = new ObservableField<>(CommonUtils.getDayfromDate(
                CommonUtils.convertUnixLongToDate(item.getDt())));
        maxTemp = new ObservableField<>(item.getMain().getTempMax().toString());
        minTemp = new ObservableField<>(item.getMain().getTempMin().toString());
        forecastDesc = new ObservableField<>(item.getWeather().get(0).getDescription());
    }

}
