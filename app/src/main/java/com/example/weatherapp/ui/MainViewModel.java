package com.example.weatherapp.ui;



import android.util.Log;

import com.example.weatherapp.data.WeatherRepository;
import com.example.weatherapp.data.WeatherRepositoryImpl;
import com.example.weatherapp.data.model.WeatherResponse;
import com.example.weatherapp.utils.CommonUtils;
import com.example.weatherapp.utils.rx.SchedulerProvider;
import com.example.weatherapp.utils.rx.SchedulerProviderImpl;

import java.util.Date;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.disposables.CompositeDisposable;

import static com.example.weatherapp.utils.CommonUtils.convertUnixLongToDate;
import static com.example.weatherapp.utils.CommonUtils.getDayfromDate;

public class MainViewModel extends ViewModel {

    private final MutableLiveData<List<WeatherResponse.List>> weatherResponseLiveData;

    private WeatherRepository weatherRepository;
    private SchedulerProvider schedulerProvider;
    private CompositeDisposable disposables;

    public MainViewModel() {
        weatherRepository = new WeatherRepositoryImpl();
        schedulerProvider = new SchedulerProviderImpl();
        disposables = new CompositeDisposable();
        weatherResponseLiveData = new MutableLiveData<>();
        fetchForecast();
    }

    /** Need to clean and extract algorithim */
    public void fetchForecast() {
        // Add loading here
        disposables.add(weatherRepository.getWeatherForecast()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(weatherResponse -> {
                    if (weatherResponse != null && weatherResponse.getList() != null) {
                        Double[] maxTemps = new Double[5];
                        Double[] minTemps = new Double[5];
                        int tempCounter = 0;
                        for (int i = 0; i < weatherResponse.getList().size() - 1; i++){
                            if (getDayfromDate(convertUnixLongToDate(weatherResponse.getList().get(i).getDt()))
                                    .equals(getDayfromDate(convertUnixLongToDate(
                                            weatherResponse.getList().get(i + 1).getDt())))){

                                if(weatherResponse.getList().get(i).getMain().getTempMax() <
                                weatherResponse.getList().get(i + 1).getMain().getTempMax()) {
                                    Double tempMax = weatherResponse.getList().get(i + 1).getMain().getTempMax();
                                }
                            } else {
                                if(maxTemps[tempCounter] != null)
                                maxTemps[tempCounter] = weatherResponse.getList().get(i).getMain().getTempMax();
                                minTemps[tempCounter] = weatherResponse.getList().get(i).getMain().getTempMin();
                            }

                        }

                        weatherResponseLiveData.setValue(weatherResponse.getList());
                    }
                }, throwable -> {
                    Log.d("testing123", "Throwable: " + throwable.getMessage());
                }));
    }

    public LiveData<List<WeatherResponse.List>> getWeatherResponseLiveData() {
        return weatherResponseLiveData;
    }


}
