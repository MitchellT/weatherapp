package com.example.weatherapp.ui;


import android.os.Bundle;

import com.example.weatherapp.R;
import com.example.weatherapp.data.model.WeatherResponse;
import com.example.weatherapp.ui.adapter.MainAdapter;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {


    private RecyclerView recyclerView;

    private MainViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        viewModel.getWeatherResponseLiveData().observe(this, new Observer<List<WeatherResponse.List>>() {
             @Override
             public void onChanged(List<WeatherResponse.List> weatherItems) {
                 MainAdapter adapter = new MainAdapter(weatherItems);
                 recyclerView.setAdapter(adapter);
             }
         });

    }
}
