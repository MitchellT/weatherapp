package com.example.weatherapp.ui.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.weatherapp.data.model.WeatherResponse;
import com.example.weatherapp.databinding.WeatherItemBinding;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.WeatherItemViewHolder> {

    private static final int VIEW_TYPE_WEATHER = 0;

    private List<WeatherResponse.List> mWeatherItemList;

    public MainAdapter(List<WeatherResponse.List> weatherItemList) {
        this.mWeatherItemList = weatherItemList;
    }

    @Override
    public int getItemCount() {
        if (mWeatherItemList!= null && mWeatherItemList.size() > 0) {
            return mWeatherItemList.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_WEATHER;
    }

    @Override
    public void onBindViewHolder(WeatherItemViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public WeatherItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_WEATHER:
                WeatherItemBinding weatherViewBinding = WeatherItemBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new WeatherItemViewHolder(weatherViewBinding);
            default:
                WeatherItemBinding defaultViewBinding = WeatherItemBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new WeatherItemViewHolder(defaultViewBinding);
        }
    }

    public void addItems(List<WeatherResponse.List> weatherItemList) {
        mWeatherItemList.addAll(weatherItemList);
        notifyDataSetChanged();
    }

    public void clearItems() {
        mWeatherItemList.clear();
    }



    public class WeatherItemViewHolder extends RecyclerView.ViewHolder {

        private WeatherItemBinding mBinding;

        private WeatherItemViewModel mWeatherItemViewModel;

        public WeatherItemViewHolder(WeatherItemBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        public void onBind(int position) {
            final WeatherResponse.List item = mWeatherItemList.get(position);
            mWeatherItemViewModel = new WeatherItemViewModel(item);
            mBinding.setViewModel(mWeatherItemViewModel);

            mBinding.executePendingBindings();
        }

    }


}