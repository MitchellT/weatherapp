package com.example.weatherapp.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CommonUtils {

    private CommonUtils(){

    }

    public static Date convertUnixLongToDate(long unix){
        return new Date((long)unix*1000);
    }

    public static String getDayfromDate(Date date){
        SimpleDateFormat simpleDateformat = new SimpleDateFormat("E");
        return simpleDateformat.format(date);
    }

    // Change to common util test

}
