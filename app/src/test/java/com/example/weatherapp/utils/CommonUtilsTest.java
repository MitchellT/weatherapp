package com.example.weatherapp.utils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Date;

import static org.junit.Assert.*;
import static org.powermock.configuration.ConfigurationType.PowerMock;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CommonUtils.class)
public class CommonUtilsTest {


    @Before
    public void setup() throws Exception {
        PowerMockito.mockStatic(CommonUtils.class);
    }

    @Test
    public void shouldCheck_dateReturned_isCorrect(){
        //CommonUtils.convertUnixLongToDate();
    }

    @Test
    public void shouldCheck_dayReturned_isCorrect(){
        Date testCase = new Date("Tue Apr 23 20:00:00 EDT 2019");
        Assert.assertSame("Tue" , CommonUtils.getDayfromDate(testCase));
    }

}